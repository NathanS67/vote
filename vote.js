let express = require('express');
let path = require('path');
let bodyParser = require('body-parser')
let crypto = require('crypto');
let ejs = require('ejs');

let app = express();

let subject = "";
let onGoingVote = false;

let pro = 0;
let con = 0;
let abstention = 0;
let notParticipating = 0;
let voters = 0;

app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/', function(req, res) {
    switch (req.query.status) {
        case "denied":
            res.render('index.ejs', { erreur: "Erreur : Accès refusé" });
            break;
        default:
            res.render('index.ejs', { erreur: "" });
    }
});

app.get('/config', function(req, res) {
    res.render('config.ejs', { subject: subject });
});

app.get('/result', function(req, res) {
    if (!onGoingVote && subject != "")
        res.render('result.ejs', { subject: subject, pro: pro, con: con, voters: voters, total: voters + abstention, notParticipating: notParticipating });
    else
        res.redirect('/?status=denied');
});

app.get('/vote', function(req, res) {
    if (onGoingVote)
        res.render('vote.ejs', { subject: subject });
    else
        res.redirect('/?status=denied');
});

app.post('/validate', function(req, res) {
    if (onGoingVote)
        switch (req.body.choice) {
            case "pro":
                pro++;
                voters++;
                break;
            case "con":
                con++;
                voters++;
                break;
            case "abstention":
                voters++;
                break;
            case "notparticip":
                notParticipating++;
                break;
            default:
                break;
        }
    res.redirect('/');
});

app.post('/validateConfig', function(req, res) {
    let users = require('./users.json');
    if (req.body.nickname == "hash")
        console.log(crypto.createHash('sha256').update(req.body.password).digest('base64'));
    else {
        users.forEach(user => {
            if (user.nickname == req.body.nickname && user.password == crypto.createHash('sha256').update(req.body.password).digest('base64')) {
                if (req.body.subject != subject) {
                    subject = req.body.subject;
                    pro = con = abstention = notParticipating = voters = 0;
                }
                switch (req.body.status) {
                    case "start":
                        onGoingVote = true;
                        break;
                    case "stop":
                        onGoingVote = false;
                        break;
                    default:
                        break;
                }
            }
        });
        res.redirect('/');
    }
});
app.listen(process.argv[2]);